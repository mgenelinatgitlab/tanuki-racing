# Tanuki Racing Release Notes

## Version: 1.0.3, Nov 15, 2023

#### Updates:
- Adding Basics workshop v1

#### Contributors:
- Logan Stucker @lfstucker

## Version: 1.0.2, Nov 15, 2023

#### Updates:
- Additional chat exercises
- Fixes to PM Auto DevOps
- Removal of old DB set up

#### Contributors:
- Rob Jackson @rjackson-gitlab
- Logan Stucker @lfstucker

# Tanuki Racing Release Notes
## Version: 1.0.1, Nov 1, 2023

#### Updates:
- Spelling corrections in the Project Management course

#### Contributors:
- Chris Guitarte @cguitarte


## Version: 1.0.0, Oct 18, 2023

#### Updates:
- Init release of new application
- Choose your own adventure type, works for any major demo of GitLab features
- Init course release comes with AI, CICD, Security, & Project Management

#### Contributors:
- Logan Stucker @lfstucker
